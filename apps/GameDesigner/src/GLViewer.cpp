/*
 * GLViewer.cpp
 *
 *  Created on: Aug 4, 2014
 *      Author: pavel
 */

#include <GLViewer.h>

GLViewer::GLViewer()
{
}

GLViewer::~GLViewer()
{
}

void GLViewer::initializeGL()
{
}

void GLViewer::paintGL()
{
}

void GLViewer::resizeGL(int width, int height)
{
}

void GLViewer::mousePressEvent(QMouseEvent *event)
{
}

void GLViewer::mouseMoveEvent(QMouseEvent *event)
{
}
