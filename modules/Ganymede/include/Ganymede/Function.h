/*
 * Function.h
 *
 *  Created on: Aug 15, 2014
 *      Author: pavel
 */

#ifndef FUNCTION_H_
#define FUNCTION_H_

namespace ganymede
{

class Function
{
public:
	Function();
	virtual ~Function();
};

} /* namespace ganymede */

#endif /* FUNCTION_H_ */
