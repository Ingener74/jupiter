/*
 * Nil.h
 *
 *  Created on: Aug 15, 2014
 *      Author: pavel
 */

#ifndef NIL_H_
#define NIL_H_

namespace ganymede
{

class Nil
{
public:
	Nil();
	virtual ~Nil();
};

} /* namespace ganymede */

#endif /* NIL_H_ */
