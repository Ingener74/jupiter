/*
 * Number.h
 *
 *  Created on: Aug 15, 2014
 *      Author: pavel
 */

#ifndef NUMBER_H_
#define NUMBER_H_

namespace ganymede
{

class Number
{
public:
	Number();
	virtual ~Number();
};

} /* namespace ganymede */

#endif /* NUMBER_H_ */
