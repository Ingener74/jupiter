/*
 * String.h
 *
 *  Created on: Aug 15, 2014
 *      Author: pavel
 */

#ifndef STRING_H_
#define STRING_H_

namespace ganymede
{

class String
{
public:
	String();
	virtual ~String();
};

} /* namespace ganymede */

#endif /* STRING_H_ */
