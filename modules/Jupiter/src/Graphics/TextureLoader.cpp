/*
 * TextureLoader.cpp
 *
 *  Created on: May 25, 2014
 *      Author: ingener
 */

#include <Jupiter/JupiterError.h>
#include <Jupiter/TextureLoader.h>

namespace jupiter
{

TextureLoader::TextureLoader()
{
}

TextureLoader::~TextureLoader()
{
}

Image TextureLoader::load()
{
    throw JupiterError("not implemented");
}

} /* namespace ndk_game */
